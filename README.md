# Flutter course (My Digital School)

## TODO Application project

![Logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/17760588/checklist.png?width=40)

Simple TODO Application made with Flutter & FireBase

## Getting Started

1. Download the project
2. Open your favorite IDE for Flutter project
3. Download the dependencies using `flutter pub get` or your IDE command shortcut
4. Run the app with `flutter run` or your IDE command shortcut
5. Enjoy the experience

---

@author LERAY Adrien

@date 2019