/*

List<Task> tasksStub = [
  Task(
    name: 'This is a task',
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    endDate: DateTime.now(),
  ),
  Task(
    name: 'Check it to make it Done',
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    endDate: DateTime.now(),
  ),
  Task(
    name: 'This task is late',
    createdDate: DateTime.utc(2020, 2, 27),
    updatedDate: DateTime.utc(2020, 2, 27),
    endDate: DateTime.utc(2020, 2, 27),
  ),
  Task(
    name: 'Check it today or later ?',
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    endDate: DateTime.utc(2020, 12, 13),
  ),
  Task(
    name: 'Again ! an other task',
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    endDate: DateTime.utc(2020, 12, 13),
  ),
  Task(
    name: 'I forgot something... but dont remember what...',
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    endDate: DateTime.utc(2020, 12, 13),
  ),
  Task(
    name: 'Hey here.. this is a task',
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    endDate: DateTime.utc(2020, 12, 13),
  ),
];

List<Category> categoriesStub = [
  Category(
    name: 'All',
    image: 'https://picsum.photos/128?date=' + DateTime.now().toString(),
    tasks: tasksStub,
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    color: Colors.amber,
  ),
  Category(
    name: 'Work',
    image: 'https://picsum.photos/128?date=' + DateTime.now().toString(),
    tasks: tasksStub,
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    color: Colors.red,
  ),
  Category(
    name: 'Music',
    image: 'https://picsum.photos/128?date=' + DateTime.now().toString(),
    tasks: tasksStub,
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    color: Colors.purple,
  ),
  Category(
    name: 'Travel',
    image: 'https://picsum.photos/128?date=' + DateTime.now().toString(),
    tasks: tasksStub,
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    color: Colors.green,
  ),
  Category(
    name: 'Study',
    image: 'https://picsum.photos/128?date=' + DateTime.now().toString(),
    tasks: tasksStub,
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    color: Colors.brown,
  ),
  Category(
    name: 'Home',
    image: 'https://picsum.photos/128?date=' + DateTime.now().toString(),
    tasks: tasksStub,
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    color: Colors.blueAccent,
  ),
  Category(
    name: 'Graphics',
    image: 'https://picsum.photos/128?date=' + DateTime.now().toString(),
    tasks: tasksStub,
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    color: Colors.deepOrange,
  ),
  Category(
    name: 'Market',
    image: 'https://picsum.photos/128?date=' + DateTime.now().toString(),
    tasks: tasksStub,
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
    color: Colors.pink,
  ),
];

 */
