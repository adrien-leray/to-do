import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Category {
  String id;
  String name;
  String image;
  DateTime createdDate;
  DateTime updatedDate;
  List<String> tasks;
  Color color;

  Category({
    this.id,
    this.name,
    this.image,
    this.createdDate,
    this.updatedDate,
    this.tasks,
    this.color,
  });

  factory Category.fromJson(
    Map<String, dynamic> json,
    DocumentSnapshot document,
  ) {
    return Category(
      id: document.documentID,
      name: json['name'],
      image: json['image'],
      createdDate: DateTime.parse(json['createdDate'].toDate().toString()),
      updatedDate: DateTime.parse(json['updatedDate'].toDate().toString()),
      tasks: json['tasks'].map<String>((task) => task.toString()).toList(),
      color: Colors.primaries[json['color']],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': this.id.toString(),
      'name': this.name.toString(),
      'image': this.image.toString(),
      'createdDate': Timestamp.fromMicrosecondsSinceEpoch(
          this.createdDate.microsecondsSinceEpoch),
      'updatedDate': Timestamp.fromMicrosecondsSinceEpoch(
          this.updatedDate.microsecondsSinceEpoch),
      'tasks': this.tasks,
    };
  }
}
