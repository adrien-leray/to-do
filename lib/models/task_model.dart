import 'package:cloud_firestore/cloud_firestore.dart';

class Task {
  String id;
  String name;
  String note;
  DateTime createdDate;
  DateTime updatedDate;
  DateTime endDate;
  bool status;

  Task({
    this.id,
    this.name,
    this.note,
    this.createdDate,
    this.updatedDate,
    this.endDate,
    this.status = false,
  });

  factory Task.fromJson(
    Map<String, dynamic> json,
    DocumentSnapshot document,
  ) {
    return Task(
      id: document.documentID,
      name: json['name'],
      note: json['note'],
      createdDate: DateTime.parse(json['createdDate'].toDate().toString()),
      updatedDate: DateTime.parse(json['updatedDate'].toDate().toString()),
      endDate: DateTime.parse(json['endDate'].toDate().toString()),
      status: json['status'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': this.name.toString(),
      'note': this.note.toString(),
      'createdDate': Timestamp.fromMicrosecondsSinceEpoch(
          this.createdDate.toLocal().microsecondsSinceEpoch),
      'updatedDate': Timestamp.fromMicrosecondsSinceEpoch(
          this.updatedDate.toLocal().microsecondsSinceEpoch),
      'endDate': Timestamp.fromMicrosecondsSinceEpoch(
          this.endDate.toLocal().microsecondsSinceEpoch),
      'status': this.status
    };
  }
}
