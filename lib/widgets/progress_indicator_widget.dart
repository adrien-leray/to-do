import 'package:flutter/material.dart';

class ProgressIndicatorWidget extends StatelessWidget {
  final String message;

  ProgressIndicatorWidget({this.message});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CircularProgressIndicator(),
          Padding(
            padding: EdgeInsets.only(
              top: 10,
            ),
            child: Text(
              message,
            ),
          )
        ],
      ),
    );
  }
}
