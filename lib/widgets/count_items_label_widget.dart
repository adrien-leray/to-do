import 'package:flutter/material.dart';

class CountItemsLabelWidget extends StatelessWidget {
  final int count;

  /// Custom text, choose it as singular word, if count > 1: add an 's' at the end of the custom text (e.g Task => Tasks)
  final String text;
  final Color color;
  final double fontSize;

  CountItemsLabelWidget({this.count, this.text, this.color, this.fontSize});

  @override
  Widget build(BuildContext context) {
    return Text(
      count.toString() + ' ' + text + (count > 1 ? 's' : ''),
      style: TextStyle(
        fontSize: fontSize,
        color: color,
        fontWeight: FontWeight.w300,
      ),
    );
  }
}
