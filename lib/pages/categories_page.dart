import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:todo/models/category_model.dart';
import 'package:todo/pages/category_page.dart';
import 'package:todo/pages/task_form_page.dart';
import 'package:todo/widgets/count_items_label_widget.dart';
import 'package:todo/widgets/progress_indicator_widget.dart';

class CategoriesPage extends StatefulWidget {
  @override
  _CategoriesPageState createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
      floatingActionButton: FloatingActionButton(
        onPressed: _navigateToTaskForm,
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _buildBody() {
    return StreamBuilder(
        stream: Firestore.instance.collection('categories').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return CustomScrollView(
              slivers: <Widget>[
                _buildAppBar(snapshot),
                _buildList(snapshot),
              ],
            );
          } else {
            return ProgressIndicatorWidget(
              message: 'Chargement des données en cours...',
            );
          }
        });
  }

  Widget _buildAppBar(AsyncSnapshot<QuerySnapshot> snapshot) {
    return SliverPadding(
      padding: EdgeInsets.only(
        top: 40,
        bottom: 10,
      ),
      sliver: const SliverAppBar(
        backgroundColor: Colors.transparent,
        titleSpacing: 30,
        title: Text(
          'Lists',
          style: TextStyle(
            fontSize: 42,
            fontWeight: FontWeight.w800,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  Widget _buildList(AsyncSnapshot<QuerySnapshot> snapshot) {
    return SliverPadding(
      padding: EdgeInsets.all(10),
      sliver: SliverGrid(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
        ),
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return _buildCategoryItem(
              Category.fromJson(
                snapshot.data.documents[index].data,
                snapshot.data.documents[index],
              ),
              index,
            );
          },
          childCount: snapshot.data.documents.length,
        ),
      ),
    );
  }

  Widget _buildCategoryItem(Category category, int index) {
    return GestureDetector(
      onTap: () {
        _navigateToItemDetail(category, index);
      },
      child: Card(
        elevation: 2,
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Hero(
                tag: 'categoryLogo' + index.toString(),
                child: Container(
                  width: 64,
                  height: 64,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      width: 4,
                      color: category.color,
                    ),
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage(
                        category.image,
                      ),
                    ),
                  ),
                ),
              ),
              Spacer(),
              Text(
                category.name,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              CountItemsLabelWidget(
                count: category.tasks.length,
                text: 'Task',
                color: Colors.black54,
                fontSize: 18,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _navigateToItemDetail(Category category, int index) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) {
          return CategoryPage(category: category, index: index);
        },
      ),
    );
  }

  void _navigateToTaskForm() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) {
          return TaskFormPage();
        },
      ),
    );
  }
}
