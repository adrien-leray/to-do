import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:todo/models/category_model.dart';
import 'package:todo/models/task_model.dart';
import 'package:todo/widgets/progress_indicator_widget.dart';

class TaskFormPage extends StatefulWidget {
  final Category category;

  TaskFormPage({this.category});

  @override
  _TaskFormPageState createState() => _TaskFormPageState();
}

class _TaskFormPageState extends State<TaskFormPage> {
  Category selectedCategory;
  DateTime selectedDate = DateTime.now();
  bool hasSelectedDate = false;
  final _formKey = GlobalKey<FormState>();

  final nameController = TextEditingController();
  final noteController = TextEditingController();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        hasSelectedDate = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    selectedCategory = widget.category;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: const Text(
          'New task',
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      body: Container(
        color: Colors.white,
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            _buildForm(),
            _buildBottomFixedSubmitButton(),
          ],
        ),
      ),
    );
  }

  Widget _buildForm() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(40),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              TextFormField(
                controller: nameController,
                keyboardType: TextInputType.multiline,
                maxLines: 4,
                maxLength: 60,
                style: TextStyle(fontSize: 26),
                decoration: InputDecoration(
                  labelText: 'What are you planning ?',
                  labelStyle: TextStyle(fontSize: 18),
                ),
                validator: (value) {
                  if (value.length == 0) {
                    return 'The title cannot be empty';
                  } else if (value.length > 60) {
                    return 'The title may not exceed 60 characters';
                  }
                  return null;
                },
              ),
              SizedBox(height: 20),
              Container(
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                        right: 15,
                      ),
                      child: Icon(
                        Icons.notifications,
                        color: Colors.grey[600],
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () => _selectDate(context),
                        child: Text(
                          hasSelectedDate
                              ? DateFormat.yMMMMd()
                                  .add_jm()
                                  .format(selectedDate)
                              : 'Plan your task (let it empty to plan for today)',
                          textAlign: TextAlign.left,
                          style:
                              TextStyle(color: Colors.grey[600], fontSize: 16),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
              TextFormField(
                controller: noteController,
                keyboardType: TextInputType.multiline,
                maxLines: 1,
                maxLength: 200,
                style: TextStyle(fontSize: 16),
                decoration: InputDecoration(
                  hintText: 'Add note',
                  icon: Icon(Icons.note),
                  border: InputBorder.none,
                ),
                validator: (value) {
                  if (value.length > 200) {
                    return 'A note may not exceed 200 characters';
                  }
                  return null;
                },
              ),
              SizedBox(height: 20),
              Container(
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                        right: 15,
                      ),
                      child: Icon(
                        Icons.local_offer,
                        color: Colors.grey[600],
                      ),
                    ),
                    StreamBuilder(
                      stream: Firestore.instance
                          .collection('categories')
                          .snapshots(),
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot> snapshot) {
                        if (snapshot.hasData) {
                          // Convert documents to categories
                          List<Category> myCategories = snapshot.data.documents
                              .map((document) =>
                                  Category.fromJson(document.data, document))
                              .toList();

                          return Expanded(
                            child: DropdownButtonFormField(
                              isExpanded: true,
                              hint: Text('Category'),
                              style:
                                  TextStyle(fontSize: 16, color: Colors.black),
                              icon: Icon(Icons.arrow_downward),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.zero,
                              ),
                              value: selectedCategory != null
                                  ? selectedCategory.id
                                  : null,
                              onChanged: (String categoryId) {
                                setState(() {
                                  for (var i = 0;
                                      i < myCategories.length;
                                      i++) {
                                    if (myCategories[i].id == categoryId) {
                                      selectedCategory = myCategories[i];
                                      break;
                                    }
                                  }
                                });
                              },
                              items: myCategories.map(
                                (Category category) {
                                  return DropdownMenuItem<String>(
                                    value: category.id,
                                    child: Text(category.name),
                                  );
                                },
                              ).toList(),
                              validator: (value) {
                                if (value == null) {
                                  return 'Category cannot be empty';
                                }
                                return null;
                              },
                            ),
                          );
                        } else {
                          return ProgressIndicatorWidget(
                            message: 'Chargement des catégories...',
                          );
                        }
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBottomFixedSubmitButton() {
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        height: 50,
        color: Colors.blue,
        child: InkWell(
          onTap: () async {
            if (_formKey.currentState.validate()) {
              // Task creation
              final task = Task(
                name: nameController.text,
                createdDate: DateTime.now(),
                updatedDate: DateTime.now(),
                endDate: selectedDate,
                note: noteController.text,
                status: false,
              );
              final DocumentReference newTask = await Firestore.instance
                  .collection("tasks")
                  .add(task.toJson());

              // Add task in category list
              final DocumentSnapshot document = await Firestore.instance
                  .collection("categories")
                  .document(selectedCategory.id)
                  .get();

              final Category category =
                  Category.fromJson(document.data, document);

              category.tasks.add(newTask.documentID);

              Firestore.instance
                  .collection("categories")
                  .document(category.id)
                  .updateData(category.toJson());

              print("Task created");
              Navigator.pop(context);
            }
          },
          child: Center(
            child: Text(
              'Create',
              style: TextStyle(
                color: Colors.white,
                fontSize: 18,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
