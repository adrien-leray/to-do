import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:todo/models/category_model.dart';
import 'package:todo/models/task_model.dart';
import 'package:todo/pages/task_form_page.dart';
import 'package:todo/widgets/count_items_label_widget.dart';
import 'package:todo/widgets/progress_indicator_widget.dart';

enum TaskState {
  LATE,
  TODO,
  DONE,
}

class CategoryPage extends StatefulWidget {
  final Category category;
  final int index;

  CategoryPage({this.category, this.index});

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  StreamController _streamController;

  Stream<QuerySnapshot> stream;

  @override
  void initState() {
    super.initState();
    setState(() {
      stream = Firestore.instance.collection('tasks').snapshots();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
      floatingActionButton: FloatingActionButton(
        onPressed: _navigateToTaskForm,
        backgroundColor: widget.category.color,
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _buildBody() {
    return StreamBuilder(
      stream: stream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasData) {
          // Get only the tasks from this category
          final filteredDocuments = snapshot.data.documents
              .where((document) =>
                  widget.category.tasks.contains(document.documentID))
              .map((document) => Task.fromJson(document.data, document))
              .toList();

          return CustomScrollView(
            slivers: <Widget>[
              _buildAppBar(),
              _buildRoundedContainer(),
              _buildListHeader('Late'),
              _buildList(
                  filteredDocuments
                      .where((task) =>
                          task.endDate.isBefore(DateTime.now()) &&
                          task.status == false)
                      .toList(),
                  TaskState.LATE),
              _buildListHeader('To Do'),
              _buildList(
                  filteredDocuments
                      .where((task) =>
                          task.endDate.isAfter(DateTime.now()) &&
                          task.status == false)
                      .toList(),
                  TaskState.TODO),
              _buildListHeader('Done'),
              _buildList(
                  filteredDocuments
                      .where((task) => task.status == true)
                      .toList(),
                  TaskState.DONE),
            ],
          );
        } else {
          return ProgressIndicatorWidget(
            message: 'Chargement des données en cours...',
          );
        }
      },
    );
  }

  /// Build the expanded app bar
  Widget _buildAppBar() {
    return SliverAppBar(
      pinned: true,
      snap: false,
      floating: false,
      expandedHeight: 250,
      backgroundColor: widget.category.color,
      flexibleSpace: FlexibleSpaceBar(
        background: Padding(
          padding: EdgeInsets.only(left: 40, bottom: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Hero(
                tag: 'categoryLogo' + widget.index.toString(),
                child: Container(
                  width: 64,
                  height: 64,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage(
                        widget.category.image,
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Text(
                  widget.category.name,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                    color: Colors.white,
                  ),
                ),
              ),
              CountItemsLabelWidget(
                count: widget.category.tasks.length,
                text: 'Task',
                color: Colors.white,
                fontSize: 28,
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Build the little hacky rounded corner effect at the top of the list
  Widget _buildRoundedContainer() {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return Container(
            color: widget.category.color,
            child: Container(
              height: 40,
              decoration: new BoxDecoration(
                color: Colors.white,
                borderRadius: new BorderRadius.only(
                  topLeft: const Radius.circular(35.0),
                  topRight: const Radius.circular(35.0),
                ),
              ),
            ),
          );
        },
        childCount: 1,
      ),
    );
  }

  /// Build header label on each list (e.g "late", "done", ...)
  Widget _buildListHeader(String value) {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.only(
              top: 10,
              bottom: 10,
              left: 40,
              right: 40,
            ),
            child: Text(
              value,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black54,
                fontWeight: FontWeight.bold,
              ),
            ),
          );
        },
        childCount: 1,
      ),
    );
  }

  /// Build the Scrollable list
  Widget _buildList(List<Task> tasks, TaskState state) {
    return SliverPadding(
      padding: EdgeInsets.only(bottom: 30),
      sliver: SliverList(
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return _buildTaskListItem(tasks[index], state, index);
          },
          childCount: tasks.length,
        ),
      ),
    );
  }

  /// Build each task item depending of the @TaskState("DONE", "LATE", ...)
  Widget _buildTaskListItem(Task task, TaskState state, int index) {
    return ListTile(
      contentPadding: EdgeInsets.only(
        top: 5,
        bottom: 5,
        left: 40,
        right: 40,
      ),
      title: Opacity(
        opacity: state == TaskState.DONE ? 0.5 : 1,
        child: Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(
            task.name,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              decoration:
                  state == TaskState.DONE ? TextDecoration.lineThrough : null,
            ),
          ),
        ),
      ),
      subtitle: Opacity(
        opacity: state == TaskState.DONE ? 0.5 : 1,
        child: Text(
          DateFormat.yMMMMd().add_jm().format(task.endDate.toLocal()),
          style: TextStyle(
            color: state == TaskState.LATE ? Colors.red[700] : Colors.black87,
            fontSize: 15,
          ),
        ),
      ),
      trailing: Checkbox(
        value: task.status,
        onChanged: (state) {
          setState(() {
            Firestore.instance
                .collection("tasks")
                .document(task.id)
                .updateData({"status": state});
          });
        },
      ),
    );
  }

  void _navigateToTaskForm() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) {
          return TaskFormPage(category: widget.category);
        },
      ),
    );
  }
}
